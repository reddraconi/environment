# environment

This is a centralization of our useful environment configurations

## ViM

We plopped in our `.vimrc` and `.vim/` directory in here. They have:

- [Dhruva Sagar's vim-table-mode](https://github.com/dhruvasagar/vim-table-mode
  "vim-table-mode")
- [Screwloose's NERDTree](https://github.com/scrooloose/nerdtree "NERDTree")
- [Tim Pope's characterize.vim](https://github.com/tpope/vim-characterize
  "characterize.vim")
- [vim-pandoc-syntax](https://github.com/vim-pandoc/vim-pandoc-syntax
  "vim-pandoc-syntax")
- [vim-pandoc](https://github.com/vim-pandoc/vim-pandoc "vim-pandoc")
- Lots of descriptive comments to explain the various bits
- Filetype-specific layout (specifically (X)/HTML, Javascript, Java, Perl, and
  PHP)
- Print (hardcopy) settings

## Bash

Our `.bashrc` is pretty minimal. It's got:

- some aliases
- a pretty PS1

## TMUX

Slightly updated to use Solarized Dark colorscheme.
