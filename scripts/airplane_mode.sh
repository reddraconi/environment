#!/bin/bash

function usage() {
  echo "Usage: $0 [on|off|status]"
  exit 1
}

function set_mode_flag() {
  export AIRPLANE_MODE=$1
}

if [ $# -lt 1 ]; then
  usage
fi

case $1 in
  on)
    sudo rfkill block all;
    rfkill ;;
#    echo 1 > set_mode_flag;;
  off)
    sudo rfkill unblock all;
    rfkill ;;
#    echo 0 > set_mode_flag ;;
  status)
    rfkill ;;
  *) usage ;;
esac
