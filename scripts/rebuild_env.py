#!/usr/bin/python

import getpass
import xml.etree.ElementTree as et
import os
import platform
import pathlib
import shutil
import subprocess
import logging
from sys import exit, executable

# Set up logging
logging.basicConfig(level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s")

# Define utility functions
def remove_file(file_path: pathlib.Path) -> None:
  """Attempt to remove a file, log error if failure occurs."""
  try:
    os.remove(file_path)
    logging.info(f"Removed file: {file_path}")
  except FileNotFoundError:
    logging.warning(f"File not found: {file_path}")
  except Exception as e:
    logging.error(f"Error removing file {file_path}: {e}")

def create_symlink(source: pathlib.Path, link_name: pathlib.Path) -> None:
  """Create a symlink if it doesn't exist."""
  try:
    os.symlink(source, link_name)
    logging.info(f"Created symlink: {link_name} -> {source}")
  except FileExistsError:
    logging.info(f"Symlink already exists: {link_name}")
  except Exception as e:
    logging.error(f"Error creating symlink {link_name}: {e}")

def install_package(package: str, installer: str, *args: str) -> None:
  """Install a package using the specified installer."""
  try:
    subprocess.run([installer, *args, package], check=True)
    logging.info(f"Successfully installed: {package}")
  except subprocess.CalledProcessError as e:
    logging.error(f"Error installing {package}: {e}")

def create_directory(directory_path: pathlib.Path) -> None:
  """Create a directory if it doesn't exist."""
  try:
    pathlib.Path(directory_path).mkdir(parents=True, exist_ok=True)
    logging.info(f"Directory created: {directory_path}")
  except Exception as e:
    logging.error(f"Error creating directory {directory_path}: {e}")

# Main script execution
def main() -> None:
  username = getpass.getuser()
  home = pathlib.Path(f"/home/{username}")
  envpath = pathlib.Path(__file__).resolve().parent.parent

  # Files and directories to check and create symlinks for
  files: list[str] = [".bashrc", ".ctags", ".dir_colors", ".gitconfig", ".style.yapf", ".vimrc"]
  directories: list[str] = [".cmus", ".config", ".vim", ".vscode", ".vim/autoload"]

  # Install required RPM packages
  rpm_file = envpath / "scripts" / "assets" / "rpms.xml"
  xml_tree = et.parse(rpm_file)
  xml_root = xml_tree.getroot()

  dnf = shutil.which("dnf")
  sudo = shutil.which("sudo")

  if not dnf or not sudo:
    logging.error("Required package manager (dnf or sudo) is not available.")
    exit(1)

  for p in xml_root.iter("package"):
    install_package(p.text, sudo, "dnf", "install", "--setopt=install_weak_deps=False", "-y")

  # Handling files: remove existing ones and create symlinks
  for f in files:
    file_path = home / f
    if file_path.is_file():
      logging.info(f"Found file {file_path}. Removing...")
      remove_file(file_path)
    create_symlink(envpath / f, file_path)

  # Handling directories: remove existing ones and recreate
  for d in directories:
    dir_path = home / d
    if dir_path.is_dir() and not dir_path.is_symlink():
      logging.info(f"Found directory {dir_path}. Removing...")
      shutil.rmtree(dir_path)
    create_directory(dir_path)

  # Copy plug.vim file
  try:
    shutil.copyfile(envpath / "scripts" / "assets" / "plug.vim", home / ".vim" / "autoload" / "plug.vim")
    logging.info("Copied plug.vim successfully.")
  except Exception as e:
    logging.error(f"Error copying plug.vim: {e}")

  # Install Python packages
  python_packages_file = envpath / "scripts" / "assets" / "python_packages.xml"
  xml_tree = et.parse(python_packages_file)
  xml_root = xml_tree.getroot()

  for p in xml_root.iter("package"):
    subprocess.run([executable, "-m", "pip", "install", p.text, "--user"], check=True)
    logging.info(f"Installed Python package: {p.text}")

  # Install VSCode extensions
  code_plugins_file = envpath / "scripts" / "assets" / "code_plugins.xml"
  xml_tree = et.parse(code_plugins_file)
  xml_root = xml_tree.getroot()

  vsc = shutil.which("code") or shutil.which("codium")

  if vsc:
    for p in xml_root.iter("plugin"):
      logging.info(f"Installing VSCode plugin: {p.text}")
      install_package(p.text, vsc, "--install-extension", "--force", "--no-sandbox", "--user-data-dir", str(home / ".vscode"))
  else:
    logging.warning("VSCode or VSCodium is not installed on this system.")

if __name__ == "__main__":
  main()

