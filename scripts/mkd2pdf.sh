#!/bin/bash

#1. Get options:
#   - source file
#   - destination file
#   - style

#1.1 Check for source file, make sure its readable
#1.2 Check for destination path, make sure its RW
#1.3 Set up a list of styles...


VERBOSE=0;
FORCE=0;
OPENPDF=0;

function usage {
  cat << EOF
  mkd2pdf -s <markdown source file> -d <destination filename> 
          -t [default|rss|github] (Style) -f force! -v verbose flag 
          -h help! (this message) -o open when complete
EOF
  exit 0;
}

function logg {
  if [ $VERBOSE -eq 1 ]; then
    echo "$1"
  fi
}

while getopts 's:d:t:vfho' opt; do
  case "${opt}" in
    s) SRC="${OPTARG}"; ;;
    d) DEST="${OPTARG}.html"; ;;
    t) STYLE="${OPTARG}"; ;;
    f) FORCE=1; ;;
    v) VERBOSE=1; ;;
    o) OPENPDF=1; ;;
    h) usage; ;;
    *) usage; ;;
  esac;
done;

logg "Checkig for source file.";
if [ ! -e "$SRC" ]; then
  echo "Cannot read ${SRC}!";
  exit 1;
else
  DIRNAME=$(dirname $(realpath "${SRC}"));
fi

logg "Checking if destination file already exists.";
if [ ! -v "${DEST}" ]; then
  DEST="${DIRNAME}/${SRC}.pdf"
  logg "Destination not set. Defaulting to '${DEST}'";
elif [ -e "$DEST" ] && [ $FORCE -eq 0 ]; then
  echo "${DEST} already exists. Won't overrite w/o '-f'!"
  exit 1;
fi


PANDOCHTML="pandoc -s -f markdown -t html -i ${SRC} -o ${DEST}";

if [ $VERBOSE == 1 ]; then
  PANDOCHTML+=" --verbose";
fi

case "${STYLE}" in
  default) 
    logg "Pandoc -> default style.";
    $PANDOCHTML -c "$ENVDIR/doc_styles/pandoc.css";
    ;;
  rss)
    logg "Pandoc -> technical style.";
    $PANDOCHTML -c "$ENVDIR/doc_styles/rss.css";
    ;;
  github)
    logg "Pandoc -> technical style.";
    $PANDOCHTML -c "$ENVDIR/doc_styles/github.css";
    ;;

  * ) 
    echo "Unknown style! Using 'default'.";
    $PANDOCHTML -c "$ENVDIR/doc_styles/pandoc.css";
    ;;
esac

if [ $OPENPDF == 1 ]; then
  xdg-open "${DEST}"
fi

exit 0;
