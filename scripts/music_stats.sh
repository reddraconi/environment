#!/bin/bash

# Helper function to extract the artist/title from JSON output or other structured data
get_json_value() {
  local json="$1"
  local key="$2"
  echo "$json" | jq -r "$key" | sed 's/"//g'  # Removing quotes
}

# Helper function to extract a specific field from CMUS output
get_cmus_tag_value() {
  local tag="$1"
  local cmus="$2"
  echo "$cmus" | grep "^tag $tag " | cut -d' ' -f 3-
}

truncate_str() {
  local str="$1"
  local len="$2"
  echo "${str:0:$len}"
}

function process_ncspot() {
  # Get the socket file location
  ncspotsock=$(lsof -c ncspot | awk '/ncspot.sock/ {print $9}')

  if [ -n "$ncspotsock" ]; then
    # Get the now playing JSON from ncspot
    now_playing=$(nc -U "$ncspotsock" -i 1 2>/dev/null)

    # Extract 'playable' field and check type
    playable_type=$(echo "$now_playing" | jq -r '.type')

    if [ "$playable_type" == "Episode" ]; then
      # Extract and truncate title
      playing_title=$(truncate_str "$title_len" "$(get_json_value "$now_playing" '.name')")
      echo -ne "[$playing_title]"
    else
      # Extract and truncate artist and title
      playing_artist=$(truncate_str "$artist_len" "$(get_json_value "$now_playing" '.artists[0]')")
      playing_title=$(truncate_str "$title_len" "$(get_json_value "$now_playing" '.title')")
      echo -ne "[$playing_artist - $playing_title]"
    fi
  fi
}

function process_moc() {
  # Get mocp process info
  mocppid=$(lsof -c mocp | awk '{print $2}')

  if [ -n "$mocppid" ]; then
    # Get the now playing info from mocp
    now_playing=$(mocp -i)

    # Extract and truncate artist and title
    playing_artist=$(truncate_str "$artist_len" "$(echo "$now_playing" | grep -i "Artist" | cut -d: -f 2-)")
    playing_title=$(truncate_str "$title_len" "$(echo "$now_playing" | grep -i "SongTitle" | cut -d: -f 2-)")

    echo -ne "[$playing_artist - $playing_title]"
  fi
}

function process_cmus() {
  # Get CMUS status
  cmus=$(cmus-remote -Q > /dev/null 2>&1)

  if [ -n "$cmus" ]; then
    # Try to get artist and title from CMUS tags
    playing_artist=$(truncate_str "$artist_len" "$(get_cmus_tag_value "artist" "$cmus")")

    if [ -z "$playing_artist" ]; then
      playing_artist=$(truncate_str "$artist_len" "$(get_cmus_tag_value "albumartist" "$cmus")")
    fi

    playing_title=$(truncate_str "$title_len" "$(get_cmus_tag_value "title" "$cmus")")

    # If still no artist, check for streaming info
    if [ -z "$playing_artist" ]; then
      streaming_track=$(echo "$cmus" | grep "^stream " | cut -d' ' -f 2-)
      playing_artist=$(truncate_str "$artist_len" "$(echo "$streaming_track" | cut -d'-' -f1)")
      playing_title=$(truncate_str "$title_len" "$(echo "$streaming_track" | cut -d'-' -f2)")
    fi

    echo -ne "[$playing_artist - $playing_title]"
  fi
}

# Run the processes
process_ncspot
process_moc
process_cmus

