#!/bin/bash

# Get terminal width and set artist/title lengths.
term_width=$(tput cols)

artist_len=$((term_width > 120 ? 40 : 20))
title_len=$((term_width > 120 ? 70 : 30))

# Truncates a string to a given length.
#
# Args:
#   length: The maximum length of the string.
#   string: The string to truncate.
#
# Returns:
#   The truncated string.
truncate() {
  local length="$1"
  local string="${2//  / }"
  local ellip=$((length - 1))

  if (( ${#string} > length )); then
    printf "%.*s…" "$ellip" "$string"
  else
    printf "%s" "$string"
  fi
}

# Get system load and memory statistics.
#
# Returns:
#   Prints the load average, free memory, and
#   total memory to stdout.
{
  read Proc _ _ < /proc/loadavg
  MemFree=$(awk '/MemAvailable/ {printf "%.1f", \
      $2/1024/1024}' /proc/meminfo)
  MemTotal=$(awk '/MemTotal/ {printf "%.0f", \
      $2/1024/1024}' /proc/meminfo)
}

printf "[$Proc|$MemFree/$MemTotal]"

# Get network interface statistics.
#
# Returns:
#   Prints the type, transmitted bytes, and
#   received bytes for each active network
#   interface.
find /sys/class/net -maxdepth 1 -mindepth 1 -not \
    -name "lo" -not -name "vir*" -print0 | while \
    IFS= read -r -d $'\0' int; do
  state=$(cat "$int/operstate" 2>/dev/null)
  if [[ "$state" == "up" ]]; then
    TX=$(( $(cat "$int/statistics/tx_bytes" \
        2>/dev/null) / 2**20 ))
    RX=$(( $(cat "$int/statistics/rx_bytes" \
        2>/dev/null) / 2**20 ))
    type=$(nmcli --get-values GENERAL.TYPE \
        device show "${int##*/}" 2>/dev/null)
    printf "[$type $TX/$RX Mb/s]"
  fi
done

# Get battery status.
#
# Returns:
#   Prints the battery state, percentage, and
#   time remaining, or "Charged" if fully
#   charged.
chassis=$(hostnamectl status 2>/dev/null | grep \
    Chassis | cut -d: -f2 | tr -dc '[[:alnum:]]')

if [[ "$chassis" == "laptop" ]]; then
  batt=$(acpi 2>/dev/null | tr -d ',' | grep \
      -v "information unavailable" | sed -E \
      's/Battery .:.(.*)/\1/' | sed -E \
      's/Discharging/-/g' | sed -E \
      's/Not Charging/x/g' | sed -E \
      's/Charging/+/g')

  if [[ ! "$batt" == *"Full"* ]]; then
    read state percent time_remaining <<< "$batt"
    printf "[$state$percent($time_remaining)]"
  elif [[ -n "$batt" ]]; then
    printf "Charged"
  fi
fi

# Processes a music player.
#
# Args:
#   player: The name of the music player.
#   cmd: The command to get the player's status.
#   artist_regex: Regex to extract the artist.
#   title_regex: Regex to extract the title.
#
# Returns:
#   Prints the artist and title, or nothing if
#   no song is playing or the player is not
#   running.
process_player() {
  local player="$1"
  local cmd="$2"
  local artist_regex="$3"
  local title_regex="$4"


  local output
  if output=$(eval "$cmd" 2>/dev/null); then
    local artist=$(echo "$output" | sed -nE "s/$artist_regex/\1/p")
    local title=$(echo "$output" | sed -nE "s/$title_regex/\1/p")

    if [[ -z "$artist" ]]; then
      artist=$(echo "$output" | cut -d'-' -f1 | xargs)
      title=$(echo "$output" | cut -d'-' -f2 | xargs)
    fi

    artist=$(truncate "$artist_len" "$artist")
    title=$(truncate "$title_len" "$title")

    if [[ -n "$artist" && -n "$title" ]]; then
      printf "[$artist - $title]"
    fi
  fi
}

# Check and process ncspot.
if lsof -c ncspot >/dev/null 2>&1; then
  flatpakcmd="flatpak run io.github.hrkfdn.ncspot info"
  flatpakvar="/run/user/$UID/.flatpak/io.github.hrkfdn.ncspot/xdg-run/ncspot"
  ncspot_sock=""
  if [ -e "$($flatpakcmd | grep -oP "USER_CACHE.*" | \
    cut -d ' ' -f2)/ncspot.sock" ]; then
    ncspot_sock="$($flatpakcmd | grep -oP "USER_CACHE.*" \
      | cut -d ' ' -f2)/ncspot.sock"
  elif [ -e $flatpakvar/ncspot.sock ]; then
    ncspot_sock=$flatpakvar/ncspot.sock
  else
    echo "DEBUG: Couldn't find ncspot.sock"
  fi

  if [[ -n "$ncspot_sock" ]]; then
    ncspot_artist=$(nc -U "$ncspot_sock" -i100ms 2>/dev/null \
      | jq '.playable.album_artists[0]')
    ncspot_title=$(nc -U "$ncspot_sock" -i100ms 2>/dev/null \
      | jq '.playable.title')

    if echo "$ncspot_artist" | grep -iq "various artist.*"; then
      ncspot_artist=$(nc -U "$ncspot_sock" -i100ms 2>/dev/null \
        | jq '.playable.album')
        ncspot_artist=$(echo $ncspot_artist | sed 's/Volume /Vol/')
    fi
    ncspot_output="$ncspot_artist ****** $ncspot_title"

    process_player "ncspot" "echo '$ncspot_output'" \
      '^"(.*)?" \*{6}.*' '^.*\*{6} "(.*)"'
  fi
fi

# Check and process moc.
if command -v mocp >/dev/null 2>&1; then
  if mocp --version >/dev/null 2>&1; then
    process_player "moc" "mocp -i" 'Artist:' 'SongTitle:'
  fi
fi

# Check and process cmus.
if command -v cmus-remote >/dev/null 2>&1; then
    if cmus-remote -Q >/dev/null 2>&1; then
        process_player "cmus" "cmus-remote -Q" \
          '^tag albumartist (.*)?' '^tag title (.*)?'
    fi
fi

#printf "\n"
