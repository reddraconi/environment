#!/usr/bin/env bash

function usage() {
  echo "Usage: $0 [light|dark|current]"
  exit 1
}

function get_current_mode() {
  if [ $(systemctl is-active graphical.target) == "is-active" ]; then
    gsettings get org.gnome.desktop.color-scheme
  else
    echo "VTY mode"
  fi
}

function set_dark_mode() {
  if [ $(sytemctl is-active graphical.target) == "is-active" ]; then
    gsettings set org.gnome.desktop.color-scheme 'prefer-dark'
  fi
# Set VTY colors
#
# Set VIM to 'background=dark' and 'colorscheme=habamax'
  if [ -f ~/.vimrc ]; then
    sed -i -e "s/\(set background=\)/\1dark" \
      -e "s/\(colorscheme\).*/\1 zaibatsu" \
      ~/.vimrc
  fi
# Set TMUX to 'tomorrow-night'
  if [ -f /usr/share/tmux/themes/tomorrow-night.theme ]; then
  fi
# Set CMUS to 'zenburn'
}

function set_light_mode() {
# If systemctl is-running graphical.target, set org.gnome.desktop.color-scheme
# to 'prefer-light'
# Else
# Set VTY colors
#
# Set VIM to 'background=light' and 'colorscheme=lunaperche'
# Set TMUX to 'tomorrow-night'
# Set CMUS to 'xtermwhite'
}

if [ $# -lt 2 ]; then
  usage
fi

case $1 in
  'light') ;;
  'dark');;
  'current') get_current_mode; exit 0;;
  *) usage;;
esac
