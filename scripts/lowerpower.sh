#!/bin/bash

current_path=$(dirname $(readlink -f $0))

## Check if 'dropbox' executable exists. ######################################
which dropbox &> /dev/null
dropbox_is_installed=$?

## Stop dropbox if it's installed #############################################

if [ $dropbox_is_installed -eq 0 ]; then
  dropbox_status=$(dropbox status | head -n 1);

  if [ "$dropbox_status" != "Dropbox isn't running!" ]; then
    echo "Stopping Dropbox."
    dropbox stop;
  else
    echo "Dropbox is not running.";
  fi
fi

## Check if SynologyDriveClient is running ####################################
if [ -d ~/.SynologyDrive ]; then
  if [ -e ~/.SynologyDrive/daemon.pid ]; then
    syno_daemon_pid=$(cat ~/.SynologyDrive/daemon.pid)
    if ps -p ${syno_daemon_pid} > /dev/null; then
      echo "Stopping SynologyDrive Client"
      kill -9 ${syno_daemon_pid}
    fi
  fi
fi

## Set backlight brightness to low ############################################

$(sudo $current_path/set_brightness.sh low)

## Update power mode to powersaving ###########################################

power_mode=$(sudo $current_path/power_mode.sh current)

if [ $power_mode != 'powersave' ]; then
  sudo "$(pwd)/power_mode.sh" powersave;
  echo "Activated PowerSave mode."
fi

## Check for SSH ##############################################################

sshd_is_running=$(systemctl is-active sshd)

if [ $sshd_is_running != 'inactive' ]; then
  echo "Disabling SSHD service."
  sudo systemctl stop sshd
fi

## Switch to text-only console ################################################

sudo systemctl isolate multi-user.target
