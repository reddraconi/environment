#!/bin/bash

BASEURL="https://www.google.com/search?q="
SEARCH_TERM=$(printf "%s%%20" "${@}")

if [ $(systemctl is-active graphical.target) = 'active' ]; then
  firefox "$BASEURL${SEARCH_TERM::-3}"
else
  elinks "$BASEURL${SEARCH_TERM::-3}"
fi
