#!/bin/bash

### GLOBALS ###################################################################

IMG="Rocky-9-GenericCloud-Base.latest.x86_64.qcow2"
BASEURL="https://download.rockylinux.org/pub/rocky/9/images/x86_64/"
CONTAINER_ID=9000

### MAIN ######################################################################

echo "## Cleaning up from previous runs"

if qm list | grep -q "^[[:space:]]\+$CONTAINER_ID"; then
  echo "Warning: Template already exists on Proxmox server. Deleting."
  qm destroy $CONTAINER_ID --purge
fi

if [ -e ./tmp/ ]; then
  rm -rf ./tmp/
fi

mkdir ./tmp/ && cd $!

# Nab files from Rocky webservers #############################################

if [ -e "./tmp/$IMG" ]; then
  rm "./tmp/$IMG"
fi

echo "## Downloading latest Rocky Image and checkum"

wget "$BASEURL$IMG"
wget "$BASEURL$IMG.CHECKSUM"

echo "## Checking download against checksum"

sha256sum --status -c $IMG.CHECKSUM

if [ $? -ne 0 ]; then
  logger "RL9 Auto Template: Rocky 9 ISO failed checksum. Bailing out!"
  echo "RL9 Auto Template: Rocky 9 ISO failed checksum. Bailing out!"
  exit 1;
fi

### Customize downloaded image ################################################

echo "## Customzing image"

virt-customize --format qcow2 -a "$IMG" \
  --instal qemu-guest-agent \
  --ssh-inject root:file:/root/blacknas/assets/ssh/id_rsa.pub \
  --update \
  --selinux-relabel

### Create template ###########################################################

echo "## Creating Proxmox VM Template"

qm create $CONTAINER_ID \
  --name "Rocky9-CloudInit-Template" \
  --memory 512 \
  --cores 2 \
  --net0 virtio,bridge=vmbr0

qm importdisk $CONTAINER_ID "$IMG" local-lvm

qm set $CONTAINER_ID \
  --scsihw virtio-scsi-pci \
  --scsi0 local-lvm:vm-$CONTAINER_ID-disk-0 \
  --boot c --bootdisk scsi0 \
  --ide2 local-lvm:cloudinit \
  --bios ovmf \
  --efidisk0 local-lvm:1,efitype=4m,pre-enrolled-keys=1,size=4M \
  --serial0 socket \
  --vga serial0 \
  --agent enabled=1

qm template $CONTAINER_ID

### Clean up ##################################################################

rm "$IMG"
exit 0
