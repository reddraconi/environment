#!/usr/bin/bash

LEN=$1

if [[ $@ -lt 1 ]]; then
  echo "Usage: $0 <password length>"
  exit 1
fi

tr -dc 'A-Za-z0-9!?%=' < /dev/urandom | head -c $1
echo
exit 0
