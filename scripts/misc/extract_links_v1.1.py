#!/usr/bin/env python

import aiohttp
import asyncio
import argparse
import logging
from bs4 import BeautifulSoup
from urllib.parse import urljoin
from typing import List, Tuple

# Set up logging configuration
logger = logging.getLogger(__name__)

async def fetch_page(session: aiohttp.ClientSession, url: str) -> Tuple[str, str]:
    """Fetch a webpage and handle redirects.

    If a redirect occurs (301, 302, 303, 307, 308), the function returns the final
    URL and the HTML content of the page.

    Args:
        session (aiohttp.ClientSession): The aiohttp session used for making the request.
        url (str): The URL to request.

    Returns:
        Tuple[str, str]: A tuple containing the final URL and the HTML content of the page.
    """
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:134.0) Gecko/20100101 Firefox/134.0"
    }

    logger.debug(f"Fetching URL: {url}")

    async with session.get(url, headers=headers) as response:
        response.raise_for_status()  # Raise an exception for bad HTTP responses (4xx, 5xx)

        # Check if the response status code indicates a redirect (e.g., 301, 302, 303, 307, etc.)
        if response.status in [301, 302, 303, 307, 308]:
            location = response.headers.get('Location')
            if location:
                logger.info(f"Redirect detected for {url} to {location}")
                # Return the redirect URL (Location header) and the HTML content
                return location, await response.text()

        # If there's no redirect or the status is not a redirect, return the original URL and the HTML content
        logger.debug(f"No redirect for {url}, returning original URL")
        return url, await response.text()

async def check_for_file_download(session: aiohttp.ClientSession, url: str) -> bool:
    """Check if a URL initiates a file download based on the Content-Disposition header.

    Args:
        session (aiohttp.ClientSession): The aiohttp session used for making the request.
        url (str): The URL to check.

    Returns:
        bool: True if the URL initiates a file download, False otherwise.
    """
    headers = {
        "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:134.0) Gecko/20100101 Firefox/134.0"
    }

    async with session.head(url, headers=headers) as response:
        response.raise_for_status()  # Raise an exception for bad HTTP responses (4xx, 5xx)

        # Check the Content-Disposition header to see if it indicates a file download
        content_disposition = response.headers.get('Content-Disposition', '')
        if 'attachment' in content_disposition.lower():
            logger.info(f"Download detected for {url}, excluding it.")
            return True

    return False

async def extract_links(url: str, suffix: str, session: aiohttp.ClientSession, exclude: List[str]) -> List[str]:
    """Extract all links from a page that end with the specified suffix, excluding certain substrings.

    Args:
        url (str): The URL of the page to extract links from.
        suffix (str): The suffix to filter links by (e.g., .pdf, .jpg).
        session (aiohttp.ClientSession): The aiohttp session used for making the requests.
        exclude (List[str]): A list of substrings to exclude from the hrefs.

    Returns:
        List[str]: A list of filtered URLs that end with the specified suffix.
    """
    # Fetch the page content asynchronously and handle redirects
    final_url, html = await fetch_page(session, url)

    logger.debug(f"Parsing HTML for {final_url}")

    # Parse the page content using BeautifulSoup
    soup = BeautifulSoup(html, 'html.parser')

    # Find all anchor tags (<a>) with an href attribute
    links = soup.find_all('a', href=True)

    # Filter the links that end with the given suffix, do not contain any excluded substrings,
    # and do not initiate a file download
    filtered_links = []
    for link in links:
        href = link['href']
        if href.endswith(suffix) and not any(excl in href for excl in exclude):
            # Ensure to join relative URLs with the base URL
            full_url = urljoin(final_url, href)  # Use final URL to join relative links

            # Check if the link leads to a file download
            is_file_download = await check_for_file_download(session, full_url)
            if not is_file_download:
                filtered_links.append(full_url)
                logger.debug(f"Found valid link: {full_url}")
            else:
                logger.debug(f"Skipping download link: {full_url}")

    return filtered_links

async def main(urls: List[str], suffix: str, exclude: List[str]) -> List[str]:
    """Process multiple URLs concurrently, extracting links with the given suffix and excluding specific substrings.

    Args:
        urls (List[str]): A list of URLs to process.
        suffix (str): The file suffix to filter links by (e.g., .pdf, .jpg).
        exclude (List[str]): A list of substrings to exclude from the hrefs.

    Returns:
        List[str]: A list of all extracted links across all pages.
    """
    async with aiohttp.ClientSession() as session:
        # Create a list of tasks for each URL
        tasks = [extract_links(url, suffix, session, exclude) for url in urls]
        # Run the tasks concurrently
        results = await asyncio.gather(*tasks)

        # Flatten the results (list of lists) into a single list
        all_links = [link for sublist in results for link in sublist]
        return all_links

def parse_args() -> argparse.Namespace:
    """Parse command-line arguments.

    Returns:
        argparse.Namespace: The parsed arguments as an argparse.Namespace object.
    """
    parser = argparse.ArgumentParser(description="Extract links with a given suffix from a list of URLs.")
    parser.add_argument('urls', metavar='URL', type=str, nargs='+',
                        help='List of URLs to process.')
    parser.add_argument('suffix', type=str,
                        help='The file suffix to filter links by (e.g., .pdf, .jpg).')

    # Add verbosity argument
    parser.add_argument('-v', '--verbose', action='store_true', help="Enable verbose logging.")

    # Add exclude argument
    parser.add_argument('--exclude', type=str, nargs='*', default=[],
                        help='List of substrings to exclude from hrefs.')

    return parser.parse_args()

if __name__ == '__main__':
    # Parse command-line arguments
    args = parse_args()

    # Set the logging level based on verbosity argument
    if args.verbose:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)

    # Log the input URLs, suffix, and exclude list
    logger.info(f"Processing URLs: {args.urls} with suffix: {args.suffix} and excluding substrings: {args.exclude}")

    # Run the async event loop with the provided URLs, suffix, and exclude list
    links = asyncio.run(main(args.urls, args.suffix, args.exclude))

    # Print the filtered links
    if links:
        logger.info("Found the following links:")
        for link in links:
            print(link)
    else:
        logger.warning(f"No links found with suffix '{args.suffix}'")

