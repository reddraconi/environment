#!/usr/bin/env python

import aiohttp
import asyncio
import argparse
from bs4 import BeautifulSoup
from urllib.parse import urljoin

# Asynchronous function to fetch a webpage
async def fetch_page(session, url):
    async with session.get(url) as response:
        response.raise_for_status()  # Raise an exception for bad HTTP responses (4xx, 5xx)
        return await response.text()

# Asynchronous function to extract links with a specific suffix
async def extract_links(url, suffix, session):
    # Fetch the page content asynchronously
    html = await fetch_page(session, url)

    # Parse the page content using BeautifulSoup
    soup = BeautifulSoup(html, 'html.parser')

    # Find all anchor tags (<a>) with an href attribute
    links = soup.find_all('a', href=True)

    # Filter the links that end with the given suffix
    filtered_links = []
    for link in links:
        href = link['href']
        # Check if the link ends with the specified suffix
        if href.endswith(suffix):
            # Make sure to join relative URLs with the base URL
            full_url = urljoin(url, href)
            filtered_links.append(full_url)

    return filtered_links

# Main function to process multiple URLs concurrently
async def main(urls, suffix):
    async with aiohttp.ClientSession() as session:
        # Create a list of tasks for each URL
        tasks = [extract_links(url, suffix, session) for url in urls]
        # Run the tasks concurrently
        results = await asyncio.gather(*tasks)

        # Flatten the results (list of lists) into a single list
        all_links = [link for sublist in results for link in sublist]
        return all_links

# Function to parse command-line arguments
def parse_args():
    parser = argparse.ArgumentParser(description="Extract links with a given suffix from a list of URLs.")
    parser.add_argument('urls', metavar='URL', type=str, nargs='+',
                        help='List of URLs to process.')
    parser.add_argument('suffix', type=str,
                        help='The file suffix to filter links by (e.g., .pdf, .jpg).')

    return parser.parse_args()

# Entry point
if __name__ == '__main__':
    # Parse command-line arguments
    args = parse_args()

    # Run the async event loop with the provided URLs and suffix
    links = asyncio.run(main(args.urls, args.suffix))

    # Print the filtered links
    if links:
        print("Found links:")
        for link in links:
            print(link)
    else:
        print(f"No links found with suffix '{args.suffix}'")

