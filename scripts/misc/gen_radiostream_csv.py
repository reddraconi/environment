#!/usr/bin/env python

import asyncio
import json
import re
import aiohttp
from aiohttp import ClientSession


async def fetch_channels(url: str, session: ClientSession) -> dict:
  """
  Fetch channel data from the provided URL asyncronously

  Args:
    url (str): The URL to fetch
    sess (ClientSession): The aiohttp session to use for requests

  Returns:
    dict: Parsed JSON
  """
  print(f"## Requesting '{url}'")
  async with session.get(url) as r:
    r.raise_for_status()
    return await r.json()


async def extract_soma_hq_playlists_from_channels(
  channels: json, session: ClientSession
) -> list:
  """
  Extract playlists from SomaFM channel data

  Args:
    channels (json): Parsed JSON data containing SomaFM channel info
    sess (ClientSession): The aiohttp session to use for requests

  Returns:
    list: List of dicts containing playlist info (title, URL, stream, format,
          quality)
  """
  playlists = []
  tasks = []  # List to store tasks for parallel execution

  for c in channels.get('channels', []):
    title = c.get('title', "NO TITLE")

    for p in c.get('playlists', []):
      playlist_info = {
        'title': f"SomaFM: {title.title()}",
        'url': p.get('url', "NO URL"),
        'stream': None,
        'format': p.get('format', "NO FORMAT"),
        'quality': p.get('quality', "NO QUALITY")
      }
      # Append only if format is 'mp3' and quality is 'high' or 'highest'
      if playlist_info['format'] == 'mp3' \
        and playlist_info['quality'] in ['high', 'highest']:
        playlists.append(playlist_info)
        # Add task to extract stream asynchronously
        tasks.append(
          extract_stream_from_playlist(p['url'], session, playlist_info)
        )

  # Run all stream extraction tasks in parallel
  await asyncio.gather(*tasks)

  return playlists


async def extract_stream_from_playlist(
  url: str, session: ClientSession, playlist_info: dict
) -> None:
  """
  Extracts first stream URL from a given playlist

  Args:
    url (str): URL of the playlist to extract from
    sess (ClientSession): The aiohttp session to use for requests
    playlist_info (dict): Dict holding playlist info

  Returns:
    None: Stream URL is added directly to the 'playlist_info' dict

  """
  async with session.get(url) as r:
    r.raise_for_status()
    if r.status == 200:
      match = re.search(r'File1.*', await r.text())
      if match:
        playlist_info['stream'] = match.group().split('=', 1)[1]
      else:
        playlist_info['stream'] = None
    else:
      print(f"Failed to fetch '{url}': Status code {r.status}")


async def fetch_nrfm_channels(channels: json, session: ClientSession) -> list:
  """
  Fetches URLs from Nightride.fm channel info

  Args:
    p (json): Parsed JSON of nightride.fm channel data
    session (ClientSession): The aiohttp session to use for requests

  Returns:
    list: A list of dicts containing nightride stream info (title, stream)
  """
  streams = []
  for s in channels.get('icestats', {}).get('source', []):
    listenurl = s.get('listenurl', None)
    servername = ((listenurl.split('/')[-1]).split('.')[0]).title()

    if listenurl and '.mp3' in listenurl.lower():
      streams.append({
        'title': f"Nightride.FM: {servername}", 'stream': listenurl
      })
  return streams


# Main async function
async def main():
  """
  Making sausage!
  """
  async with aiohttp.ClientSession() as session:
    # Fetch channels concurrently
    somafm_channels = await fetch_channels(
      "https://somafm.com/channels.json", session
    )
    nightridefm_channels = await fetch_channels(
      "https://stream.nightride.fm/status-json.xsl", session
    )
    manual_channels = [
      {
        'title': 'Megaton Café Radio',
        'stream': 'https://45.79.186.124/proxy/megatoncafe/stream'
      },
      {
        'title':
          'Mississippi Public Broadcast (MPB) Radio: Talk Radio',
        'stream':
          'https://playerservices.streamtheworld.com/api/livestream-redirect/WMPNHD3_128.mp3'
      },
      {
        'title':
          'Mississippi Public Broadcast (MPB) Radio: Music Radio',
        'stream':
          'http://playerservices.streamtheworld.com/api/livestream-redirect/WMPNFM_56.mp3'
      }
    ]

    # Extract playlists concurrently
    playlists = await extract_soma_hq_playlists_from_channels(
      somafm_channels, session
    )
    nrfm_streams = await fetch_nrfm_channels(nightridefm_channels, session)

    # Print results
    print("Title, Stream URL")
    channels = sorted((playlists + nrfm_streams + manual_channels),
                      key=lambda d: d['title'])
    for channel in channels:
      print(f"\"{channel['title']}\",\"{channel['stream']}\"")


# Run the async main function
if __name__ == "__main__":
  asyncio.run(main())
