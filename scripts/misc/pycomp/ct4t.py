#!/usr/bin/env python3

import argparse
from datetime import datetime, timedelta, time
from pathlib import Path
from zoneinfo import ZoneInfo
from argparse import ArgumentParser
from enum import Enum
from operator import ior
from typing import List
from functools import reduce
import logging
import os
import tomllib
import sys
import json

## Application Logging Setup ###################################################

logg = logging.getLogger(__name__)
logg.setLevel(logging.DEBUG)

con_handler = logging.StreamHandler()
con_handler.setLevel(logging.WARNING)
console_formatter = logging.Formatter(
  '%(asctime)s - %(levelname)s - %(message)s'
)
con_handler.setFormatter(console_formatter)
logg.addHandler(con_handler)

file_handler = logging.FileHandler('ct4t.log')
file_handler.setLevel(logging.DEBUG)
file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
file_handler.setFormatter(file_formatter)
logg.addHandler(file_handler)

## Application Logging Setup Complete ##########################################

## Globals #####################################################################

_interactive_mode = False
_verbose_mode = False
_traveler_info = {}
_holidays = {}
_trip_info = {}

## Globals Complete ############################################################

## Enums #######################################################################


class output_file_formats(Enum):
  TOML = "toml"
  YAML = "yaml"
  JSON = "json"
  CSV = "csv"
  EXCEL = "excel"
  PDF = "pdf"


class filesystem_permissions(Enum):
  READ = "r"
  WRITE = "w"
  EXECUTE = "x"


## Enums Complete ##############################################################

## Functions ###################################################################

### Argument Parsing


def process_arguments() -> dict:

  parser = argparse.ArgumentParser(description="Comptime for Travel Calculator")
  parser.add_argument(
    '-c',
    '--travelerconfig',
    help=
    "Traveler configuration. Must be valid TOML. See example 'traveler.toml' for more info."
  )
  parser.add_argument(
    '-i',
    '--input',
    help="Input (Trip) file. Must be valid YAML.",
    type=argparse.FileType('r')
  )
  parser.add_argument(
    '-o',
    '--output',
    help="Filename to save output.",
    required=True,
    type=argparse.FileType('w')
  )
  parser.add_argument(
    '-of',
    '--outputformat',
    help="Output format. Must be 'CSV', 'XLS', or 'PDF'. XLS by default.",
    choices=['CSV', 'XLS', 'PDF'],
    default='XLS'
  )
  parser.add_argument(
    '-a',
    '--calendarfile',
    help="TOML of official and command-specific holidays.",
    type=argparse.FileType('r')
  )
  parser.add_argument(
    '-v',
    '--verbose',
    help="Enable VERBOSE logging. Disabled by default.",
    action='store_true'
  )

  args = parser.parse_args()
  if args.verbose:
    _verbose_mode = True

  return args


### Check for filesystem objects


def object_exists_with_perms(
  type: str, filepath: str, perms: List[str]
) -> bool:
  """ Check that the requested object exists and has the desired permissions

  Args:
    type(str): The type of thing to look for (file or path)
    filepath(str): The thing to look for
    perms(List[str]): The required permissions. This is set to [os.R_OK] by
      default. (Optional)

  Returns:
    bool: True if the file exists and is readable. False otherwise
  """
  if type not in ["file", "path"]:
    logg.warn(
      f"Unknown type '{type}' requested when looking for a filesystem object. "
      f"Defaulting to 'file'."
    )
    type = "file"

  if not perms and type == 'file':
    perms = [os.R_OK]
  elif not perms and type == 'path':
    perms = [os.R_OK, os.X_OK]

  try:
    if type == 'file':
      if not os.path.isfile(filepath):
        logg.warn(f"Unable to fild the file '{filepath}'.")
        return False
    if type == 'path':
      if not os.path.isdir(filepath):
        logg.warn(f"Unable to fild the file '{filepath}'.")
        return False

    return os.access(filepath, reduce(ior, perms))
  except Exception as e:
    logg.warning(
      f"Something bad happened when validating {type} '{filepath}': {e}"
    )
    return False


### Input Parsing

### File Parsers

#### Holiday TOML


def load_holidays(calendar: str) -> dict:
  if not calendar:
    logg.info(
      "No calendar file provided. Comptime that is earned during a workday "
      "that also falls on a holiday will not be correctly calculated."
    )


#### Traveler Information / User Config TOML


def load_user_config(user_config: str) -> dict:

  defaults = {
    "TravelerName": "NONAME",
    "AverageCommuteInHours": 0.75,
    "FLSAExempt": False,
    "WorkweekStartDay": "Monday",
    "WorkweekEndDay": "Friday",
    "WorkdayStartTime": "08:00",
    "WorkdayEndTime": "16:30",
    "CommandTravelCompTimeIncrement": 0.10
  }

  if not user_config:
    logg.info(
      "No traveler configuration provided. Using the following defaults: "
      "Traveler Name: NONAME    Average Commute Time: 0.75h    "
      "FLSA Exempt: False    Work Week Start Day: Monday    "
      "Work Week End Day: Friday    Work Day Start Time: 0800    "
      "Work Day End Time: 1630    Command Comptime Increment: 0.1h"
    )
    return defaults

  try:
    with open(user_config, 'r') as f:
      config = tomllib.load(f)

    return config
  except Exception as e:
    logg.warning(
      f"Unable to load requested traveler configuration '{user_config}'"
      f": {e}. Setting to {defaults}."
    )
    return defaults


#### Trip YAML


def load_trip(trip: str) -> dict:
  if not trip:
    logg.info("No trip file provided. Switching to interactive mode.")
    interactive_input()
  else:
    try:
      with open(trip) as f:
        trip_entries = json.load(trip)
    except Exception as e:
      logg.error(
        f"Unable to process trip file '{trip}': {e}. "
        f"Switching to interactive mode."
      )
      interactive_input()

  # We only get here if the yaml file was properly parsed.
  # Check for 'TripIdentifier', 'TripStartDate' and 'TripEndDate'.
  # If 'TripIdentifier' is missing, set it to 'MISSING TRIP ID'
  # If 'TripStartDate' is missing, but the 'Departure' block processes
  #  correctly, set the 'TripStartDate' to the first record's
  #  'DepartureDateTime'
  # TODO: Is this correct?
  # If 'TripEndDate' is missing, but the 'Return' block processes correctly, set
  # the 'TripEndDate' to the last record's 'ArrivalDateTime'.
  pass


def interactive_input():
  print("Interactive input.")
  pass


def save_ct4t(outputfile):
  pass


### main
def main():  #
  args = process_arguments()
  load_holidays(args.calendarfile)
  load_user_config(args.travelerconfig)
  load_trip(args.input)
  save_ct4t(args.output)


## Functions Complete ##########################################################

if __name__ == "__main__":
  logg.info("Executing Comptime for Travel Calculator.")
  main()
  logg.info("Comptime for Travel Calculator execution complete.")
  sys.exit(0)
