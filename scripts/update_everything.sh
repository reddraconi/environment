#!/usr/bin/env bash

function usage() {
  echo "Usage: $0 <parent directory of all git repos>"
  exit 1
}

if [ $EUID -ne 0 ]; then
  echo "This script requires root privileges. Run as 'root' or with 'sudo'."
  usage
fi

if [ -z "$1" ]; then
  echo "This script requires a parent directory with one or more git subdirs."
  usage
fi

## Update system packages with root privileges

#dnf update -y

## Drop root privilges
## update flatpaks and <>, if they're available
## TODO: check for flatpak and also add support for the other package thing.
#flatpak update

readarray -d '' git_directories < \
  <(find "$1" -maxdepth 1 -type d \! -path "$1" -prune -print0)

for d in "${git_directories[@]}"; do
  echo "------------------------- Updating '${d}'"
  cd "${d}"
  setpriv --reuid=$SUDO_USER --regid=$SUDO_USER --init-groups --inh-caps=-all \
    git pull
done

exit 0
