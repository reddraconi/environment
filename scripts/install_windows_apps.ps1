$apps = Get-Content -raw ".\scripts\winget_requirements.json" | ConvertFrom-Json

foreach ($a in $apps.apps) {
  Write-Host("Installing $($a)")
  winget install $a
}