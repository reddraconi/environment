#!/bin/bash

AMD_BKL=/sys/class/backlight/amd*
INTEL_BKL=/sys/class/backlight/intel*
NVIDIA_BLK=/sys/class/backlight/nvidia*

function usage() {
  echo "Usage: $0 [min|low|medium|high|max|<integer>]"
  exit 1
}

if [ $UID -ne 0 ]; then
  echo "This script requires root privileges. Run as 'root' or with 'sudo'."
  usage
fi

function get_vendor() {
  if [ -e $AMD_BKL ]; then
    echo "AMD"
  elif [ -e $INTEL_BKL ]; then
    echo "INTEL"
  elif [ -e $NVIDIA_BKL ]; then
    echo "NVIDIA"
  else
    echo "UNKNOWN"
  fi
}

function get_max_brightness() {
  if [ $1 != "UNKNOWN" ]; then
    echo $(cat /sys/class/backlight/${1,,}*/max_brightness)
  else
    exit 1
  fi
}

function set_brightness() {
  MAX_BR=$(get_max_brightness $2)
  BR_FILE=/sys/class/backlight/${2,,}*/brightness
  MIN_BR=$(bc <<< "$MAX_BR * 0.1" | cut -d. -f1)
  LOW_BR=$(bc <<< "$MAX_BR * 0.25" | cut -d. -f1)
  MED_BR=$(bc <<< "$MAX_BR * 0.40" | cut -d. -f1)
  HI_BR=$(bc <<< "$MAX_BR * 0.80" | cut -d. -f1)

  case "$1" in
    min) echo $MIN_BR > $BR_FILE;;
    low) echo $LOW_BR > $BR_FILE;;
    medium) echo $MED_BR > $BR_FILE;;
    high) echo $HI_BR > $BR_FILE;;
    max) echo $MAX_BR > $BR_FILE;;
    "[::digit::]");;
    *)
      echo "Unrecognized input to set brightness!";
      echo "Defaulting to medium brightness.";
      set_brightness medium;
      ;;
  esac
}

VENDOR=$(get_vendor)
MAX_BR=$(get_max_brightness $VENDOR)

if [ -z $1 ]; then
  usage
fi

set_brightness $1 $VENDOR

