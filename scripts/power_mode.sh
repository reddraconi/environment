#!/usr/bin/env bash

ALL_SCALING_GOV="/sys/devices/system/cpu/cpu*/cpufreq/scaling_governor"
SCALING_GOV_0="/sys/devices/system/cpu/cpu0/cpufreq/scaling_governor"

function usage(){
  echo "Usage: $0 [performance|schedutil|ondemand|conservative|powersave|userspace|current]"
  exit 1
}

if [ $UID -ne 0 ]; then
  echo "This script requires root privileges. Run as 'root' or with 'sudo'."
  usage
fi

function getcpumode() {
  cat "$SCALING_GOV_0"
}

function setcpumode() {
  echo $1 | tee ${ALL_SCALING_GOV}
}

case "$1" in
  performance) setcpumode performance; exit 0;;
  schedutil) setcpumode schedutil; exit 0;;
  ondemand) setcpumode ondemand; exit 0;;
  conservative) setcpumode conservative; exit 0;;
  powersave) setcpumode powersave; exit 0;;
  userspace) setcpumode userspace; exit 0;;
  current) getcpumode; exit 0;;
  *) echo "Urecognized mode: $1"; usage;;
esac
