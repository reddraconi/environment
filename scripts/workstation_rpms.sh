#!/bin/bash

if [ $EUID -ne 0 ]; then
  echo "This script requires sudo or root privs. Try again.";
  exit 1;
fi

distro=$(grep "^ID=" /etc/os-release | cut -d= -f2)

if [ ! "$distro" == 'fedora' ]; then
  echo "This script is only for Fedora. You've got '$distro'. :("
  exit 1;
fi

## Dropbox

#if [ ! -e /etc/yum.repos.d/dropbox.repo ]; then
#
#  cat <<EOF > /etc/yum.repos.d/dropbox.repo
#[Dropbox]
#name=Dropbox Repository
#baseurl=http://linux.dropbox.com/fedora/$releasever/
#gpgkey=https://linux.dropbox.com/fedora/rpm-public-key.asc
#EOF
#
#fi

## VSCode

if [ ! -f /etc/yum.repos.d/vscode.repo ]; then

  cat <<EOF > /etc/yum.repos.d/vscode.repo
[code]
name=Visual Studio Code
baseurl=https://packages.microsoft.com/yumrepos/vscode
enabled=1
gpgcheck=1
gpgkey=https://packages.microsoft.com/keys/microsoft.asc
EOF

fi
## RPM Fusion

if [ ! -f /etc/yum.repos.d/rpmfusion-free.repo ]; then

  cat <<EOF > /etc/yum.repos.d/rpmfusion-free.repo
[rpmfusion-free]
name=RPM Fusion for Fedora $releasever - Free
#baseurl=http://download1.rpmfusion.org/free/fedora/releases/$releasever/Everything/$basearch/os/
metalink=https://mirrors.rpmfusion.org/metalink?repo=free-fedora-$releasever&arch=$basearch
enabled=1
metadata_expire=14d
type=rpm-md
gpgcheck=1
repo_gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-rpmfusion-free-fedora-$releasever
EOF

  cat <<EOF > /etc/yum.repos.d/rpmfusion-free-updates.repo
[rpmfusion-free-updates]
name=RPM Fusion for Fedora $releasever - Free - Updates
#baseurl=http://download1.rpmfusion.org/free/fedora/updates/$releasever/$basearch/
metalink=https://mirrors.rpmfusion.org/metalink?repo=free-fedora-updates-released-$releasever&arch=$basearch
enabled=1
enabled_metadata=1
type=rpm-md
gpgcheck=1
repo_gpgcheck=0
gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-rpmfusion-free-fedora-$releasever
EOF

fi

if [ ! -f /etc/yum.repos.d/rpmfusion-nonfree-nvidia-driver.repo ]; then

  cat <<EOF > /etc/yum.repos.d/rpmfusion-nonfree-nvidia-driver.repo
[rpmfusion-nonfree-nvidia-driver]
name=RPM Fusion for Fedora $releasever - Nonfree - NVIDIA Driver
#baseurl=http://download1.rpmfusion.org/nonfree/fedora/nvidia-driver/$releasever/$basearch/
metalink=https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-nvidia-driver-$releasever&arch=$basearch
enabled=1
enabled_metadata=1
type=rpm-md
gpgcheck=1
repo_gpgcheck=0
gpgkey=file:///usr/share/distribution-gpg-keys/rpmfusion/RPM-GPG-KEY-rpmfusion-nonfree-fedora-$releasever
skip_if_unavailable=True
EOF

fi

if [ ! -f /etc/yum.repos.d/rpmfusion-nonfree-steam.repo ]; then

  cat <<EOF > /etc/yum.repos.d/rpmfusion-nonfree-steam.repo
[rpmfusion-nonfree-steam]
name=RPM Fusion for Fedora $releasever - Nonfree - Steam
#baseurl=http://download1.rpmfusion.org/nonfree/fedora/steam/$releasever/$basearch/
metalink=https://mirrors.rpmfusion.org/metalink?repo=nonfree-fedora-steam-$releasever&arch=$basearch
enabled=1
enabled_metadata=1
type=rpm-md
gpgcheck=1
repo_gpgcheck=0
gpgkey=file:///usr/share/distribution-gpg-keys/rpmfusion/RPM-GPG-KEY-rpmfusion-nonfree-fedora-$releasever
skip_if_unavailable=True
EOF

fi

for file in /etc/yum.repos.d/*copr*.repo; do
  rm "$file"
done

if [ -e /usr/bin/chrome ]; then
  dnf remove google-chrome -y

  if [ -e /etc/yum.repos.d/google-chrome.repo ]; then
    rm /etc/yum.repos.d/google-chrome.repo
  fi
fi

dnf update -y
dnf install \
  code \
  ghc-ShellCheck \
  golang-bin \
  moolticute \
  openssh-askpass \
  pylint \
  thunderbird \
  vim \
  vlc \
  --setopt=install_weak_deps=False

go install github.com/raoulh/mc-agent@latest
cp ./mc-agent /usr/bin/.
