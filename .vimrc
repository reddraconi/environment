" ----------------------------------------------------------------------------
" General Vim/gVim configuration
" ----------------------------------------------------------------------------

" Disable compatibility mode
set nocompatible

" Remove options to keep things from breaking
set sessionoptions-=options

" Line Numbering
set nu

" Undo History
set history=700

" Syntax Highlighting & Indentation
filetype plugin on
filetype indent on

" Automatically update Vim buffer if file was changed elsewhere. If it was
" deleted, then vim just keeps a copy in the local buffer until you save or toss
" it.
set autoread

" Default shell to bash, but overwrite if we're running on Windows
set shell=bash
if has('win32')
  set shell=powershell
endif
if has('win32unix') "Cygwin
  set shell=bash
endif

" Keep 7 lines above & below cursor
set so=7

" Set autocomplete options for commands and such
set completeopt=menuone,noselect

" Enables Wild menu for autocomplete. Ignores *.o, backup, and *.pyc files
"  Sorts wild entries by longest first.
set wildmenu
set wildmenu wildmode=list:longest,full
set wildignore+=tags,*.o,*~,*.pyc,*.un
set wildignore+=**/.git,**/.svn,**/Thumbs.db,**/node_modules,**/.vscode
set wildignorecase

" Sets hight of command line bar to 2
set cmdheight=2

" hides buffers rather than kill them when doing stuff like :e.
set hid

" Make backspace work like WYSIWYG editors.
set backspace=eol,start,indent

" Allows cursors to wrap left and right with cursors keys or h/l in normal
"   mode and cursor keys in insert mode.
set whichwrap+=<,>,h,l,[,]

" Add angle brackets to matchpair list. Let's you move between matching
"   pairs using % in normal mode
set matchpairs+=<:>

" Expands tabs to spaces, sets 2 spaces to a tab and uses 2 spaces when
"   re-indenting stuff.
set expandtab
set shiftwidth=2
set tabstop=2

" Ignore case when searching, highlight found matches, and moves the
"   line to the nearest string that matches. Great for using regular
"   expressions
set ignorecase
set hlsearch
set incsearch

" Enables magic mode for regular expressions. Keeps us from escaping a
"   million characters. Note: this will jack you up if you're use to the
"   standard POSIX regular expressions
set magic

" Automatically highlights matching brackets when your cursor over them,
"   and makes them blink for easy visibility
set showmatch
set mat=2

" Uses default syntax methods for code solding. Fold all things after
"   level one and only allow one set of folds to hold another.
set foldmethod=syntax
set foldlevelstart=4
set foldnestmax=2
set foldopen=hor,insert,jump,mark,percent,quickfix,search,tag,undo

" Our terminal can handle 256 colors. Make the background dark.
set t_Co=256

" Set some optional stuff to enhance tmux a bit.
let &t_Cs = "\e[4:3m"
let &t_Ce = "\e[4:0m"

" Underdoubles.
let &t_Us = "\e[4:2m"
let &t_ds = "\e[4:4m"
let &t_Ds = "\e[4:5m"

" 256 colors.
let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"

" Sets a line of color at column 80. Because 80. 120 works well when we're
" doing (X)HTML/XML and PowerShell stuff. Also, we set the default text width to
" 80 columns. We also disable the double-space after a period when reformatting
" docs.
set cc=80
set tw=80
set nojoinspaces

" Enables syntax highlighting. Arguably the best part of Vim.
syntax enable
set synmaxcol=200

" Sets default file encoding and line endings. We typically hang out in
"   *NIX land, so UNIX works well for us.
set encoding=utf8
set ffs=unix,dos

" Changes some of the default hidden characters that show when you use
"   :list. Stolen from Tim Pope's .vimrc
"   (https://github.com/tpope/tpope/blob/master/.vimrc)
if (&termencoding ==# 'utf-8' || &encoding ==# 'utf-8') && v:version >= 700
  let &g:listchars = "tab:\u21e5\u00b7,trail:\u2423,extends:\u21c9,precedes:\u21c7,nbsp:\u00b7"
  let &g:fillchars = "vert:\u250b,fold:\u00b7"
else
  setglobal listchars=tab:>\ ,trail:-,extends:>,precedes:<
endif
" Enable word wrap and prevent it from breaking words in the middle
set wrap
set lbr

" Enable autoindent
set ai


" ----------------------------------------------------------------------------
" gVim configuration
" ----------------------------------------------------------------------------
if has ('gui_running')
  " Remove toolbar, disable the right-hand scrollbar and menubar
  set guioptions-=T
  set guioptions-=r
  set guioptions-=m

  " Set tab labels to <modification status> <filename>
  set guitablabel=%M\ %t

  " Configure short message string
  set shortmess=aTItoO

  " Sets the Window title on by default. The default string is:
  "   <filename> <modification status> <file path> - GVIM
  set title

  " Set font. We use Ligconsolata 'cause it's awesome and easy to read.
  "  Plus, who doesn't love slashed zeroes and nice programmery things?
  set guifont=Ligconsolata:h15:qANTIALIASED

  " Set undercurl for mispelled words.
  let s:c = ",undercurl"

  " Set clipboard to system clipboard instead of internal Vim clipboard for
  " sanity when using gvim
  set clipboard=unnamedplus
endif

" Turn off bells. We know when we've done wrong.
set noerrorbells visualbell t_vb=

" Disable mouse control by Vim.
set mousehide
set mousem=

" Configure spelling language and related settings
set spelllang=en_us
set spellsuggest=best,5

" Configure hidden character settings. We're checking first to see if the
"  terminal we're using supports UTF-8 characters. If it doesn't, we use
"  old-school cool ASCII versions.

if (&termencoding ==# 'utf-8' || &encoding ==# 'utf-8') && version >= 700
  set listchars=tab:›\
  set listchars+=eol:¬
  set listchars+=trail:⋅
  set listchars+=extends:›
  set listchars+=precedes:‹
  set listchars+=nbsp:+

  set fillchars=stl:\
  set fillchars+=stlnc:\
  set fillchars+=vert:\|
  set fillchars+=fold:\⋅
  set fillchars+=diff:-

  set showbreak=↪\
else
  set listchars=tab:\ \
  set listchars+=eol:$
  set listchars+=trail:~
  set listchars+=extends:>
  set listchars+=precedes:<
  set listchars+=nbsp:+

  set fillchars=stl:\
  set fillchars+=stlnc:\
  set fillchars+=vert:\|
  set fillchars+=fold:\-
  set fillchars+=diff:-

  set showbreak=->\
endif

" ----------------------------------------------------------------------------
" Colorscheme settings
" ----------------------------------------------------------------------------
" Ensure that spelling mistakes are _always_ highlighted, no matter the
" colorscheme
" Fix issues with TERM being set to anything with '256color'
if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
endif
set background=dark
colorscheme zaibatsu
hi colorcolumn ctermbg=darkgreen guibg=darkgreen
hi Normal guibg=NONE ctermbg=NONE
hi NonText ctermbg=none

" ----------------------------------------------------------------------------
" Window management
" ----------------------------------------------------------------------------

" Automatically resize splits when the window size changes
autocmd VimResized * tabdo wincmd =

" ----------------------------------------------------------------------------
" Plugin management
" ----------------------------------------------------------------------------
if has('win32')
  call plug#begin("$HOME/vimfiles/plugged")
else
  call plug#begin("$HOME/.vim/plugged")
endif
Plug 'dense-analysis/ale', {'for': ['javascript','typescript, pandoc, markdown']}
Plug 'dhruvasagar/vim-table-mode', {'for': ['pandoc', 'markdown']}
Plug 'dpelle/vim-LanguageTool', {'for': ['pandoc','markdown']}
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-surround'
Plug 'majutsushi/tagbar'
Plug 'vim-pandoc/vim-pandoc', {'for': ['pandoc', 'markdown']}
Plug 'vim-pandoc/vim-pandoc-syntax', {'for': ['pandoc', 'markdown']}
Plug 'vim-voom/VOoM', {'for': ['pandoc', 'markdown']}
call plug#end()

" ----------------------------------------------------------------------------
" vim-pandoc configuration
" ----------------------------------------------------------------------------
let g:pandoc#folding#fdc=0

" ----------------------------------------------------------------------------
"  LanguageTool configuration
"  ---------------------------------------------------------------------------
let g:languagetool_jar='/usr/local/bin/LanguageTool/languagetool-commandline.jar'
let g:languagetool_lang='en-US'
let g:languagetool_disable_rules='PROFANITY,TH_THORIUM,Y_ALL,WATCHA,WANNA,GONNA,OUTTA,EN_QUOTES,DASH_RULE'
hi LanguageToolGrammarError  guisp=blue gui=undercurl guifg=NONE guibg=NONE ctermfg=white ctermbg=blue term=underline cterm=none
hi LanguageToolSpellingError guisp=red  gui=undercurl guifg=NONE guibg=NONE ctermfg=white ctermbg=red  term=underline cterm=none

" ----------------------------------------------------------------------------
" ViM table-mode configuration
"   Use Pipes to make it markdown-compatible
" ----------------------------------------------------------------------------
let g:table_mode_corner='|'

" ----------------------------------------------------------------------------
" taglist-specific settings
" ----------------------------------------------------------------------------
let Tlist_Inc_Winwidth=0
let Tlist_Exit_OnlyWindow=1
let Tlist_Use_Horiz_Window=1
let Tlist_Compact_Format=1
let Tlist_WinHeight=8

" ----------------------------------------------------------------------------
" ALE Configurations
" ----------------------------------------------------------------------------

let g:ale_linters = {
      \ 'javascript': ['eslint'],
      \ 'python': ['yapf'],
      \}

let g:ale_echo_msg_format = '[%linter%] %s [%severity]'
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'
let g:ale_lint_on_text_changed = 'never'
let g:indentLine_char = '⦙'

" ----------------------------------------------------------------------------
" Key maps
" ----------------------------------------------------------------------------

" Do a quick `ls` of the current working directory. This /may/ work if
"       you're on Windows and have 'set shell=powershell'
map <F3> :!ls<CR>:e

" Set a toggle for the list command. Handy when working with files that
"       are doing strange things...
map <F4> :set list!<cr>

" Toggles spell checking. Great for working with markdown files.
map <F5> :setlocal spell! spelllang=en_us<cr>

" Toggles NerdTree split in normal mode
nnoremap <silent><F6> :NerdTreeToggle<cr>

" ----------------------------------------------------------------------------
" Functions and auto commands
" ----------------------------------------------------------------------------

" A nice, human-redable output of the current file's size.
function! FileSize()
    let bytes = getfsize(expand("%:p"))
        if bytes <= 0
                return ""
    endif
    if bytes < 1024
        return "b"
    else
        return (bytes/1024)."k"
    endif
endfunction

" ----------------------------------------------------------------------------
" Statusline configuration
" ----------------------------------------------------------------------------

" Make sure the statusline is always visible
set laststatus=2

" We don't want to lose any data from the shortmessage
set shm=a

" Modstatus of current file in #constant# color
set statusline=%#constant#\ %m

" The filename, file flags, and the file type in the #underlined# color
set statusline+=%#underlined#\ %t%h%r%y

" The filesize, calculated by FileSize() in the #special# color
set statusline+=\ %#special#%{FileSize()}

" Right-aligned the current line / total lines and the current column in the
" #preproc# color
set statusline+=\ %=%#preproc#\ [L:%l/%L][C:%c]

" The cursor's percentage in the #comment# color
set statusline+=\ %#comment#%P\

" ----------------------------------------------------------------------------
" Command Aliases
" ----------------------------------------------------------------------------

command LTCH LanguageToolCheck
command LTCL LanguageToolClear

" ----------------------------------------------------------------------------
" Autocommands
" ----------------------------------------------------------------------------

" Automatically remove whitespace at the end of lines when writing files
autocmd BufWritePre * :%s/\s\+$//e

" Removes the background highlight for inactive splits. Making it easier
"   to find the active one.
augroup BgHilight
    autocmd! BgHilight
    autocmd WinEnter * set cul
    autocmd WinLeave * set nocul
augroup END

" Setup automatic tabbing for YAML and Python
autocmd FileType gitcommit setlocal tw=72
autocmd FileType html setlocal ts=2 sts=2 sw=2 expandtab tw=120 cc=120
autocmd FileType markdown setlocal ts=2 sts=2 sw=2 expandtab tw=120 cc=120
autocmd FileType pandoc setlocal ts=2 sts=2 sw=2 expandtab tw=120 cc=120
autocmd FileType python setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType xml setlocal ts=2 sts=2 sw=2 expandtab tw=120 cc=120
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" Setup automatic syntax highlighting for '*.tab.txt' files:
autocmd BufRead,BufNewFile *.tab.txt set filetype=chordlyric

" ----------------------------------------------------------------------------
" Hardcopy / Printer Configuration
" ----------------------------------------------------------------------------

" Set header for printouts to truncate, if necessary the filename, file
"   flags, filetype. The Page # goes to the right-hand side.
set printheader="%<%f%h%m%=Page %N"

" Set header to two lines, the output to black-and-white, remove line
"   numbers, and set the default paper size to US letter (8.5" x 11")
set printoptions=header:2,syntax:n,number:n,paper:letter

" ----------------------------------------------------------------------------
" Setup helptags and such.
" ----------------------------------------------------------------------------
packloadall
silent! helptags ALL
