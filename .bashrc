# .bashrc
# vim: ft=sh

# Set linux framebuffer colors to ones we can actually read
if [ "$TERM" = "linux" ]; then
    echo -en "\e]P0181818" #black
    echo -en "\e]P8222222" #darkgrey
    echo -en "\e]P1803232" #darkred
    echo -en "\e]P9982b2b" #red
    echo -en "\e]P25b762f" #darkgreen
    echo -en "\e]PA89b83f" #green
    echo -en "\e]P3aa9943" #darkyellow
    echo -en "\e]PBefef60" #yellow
    echo -en "\e]P4324c80" #darkblue
    echo -en "\e]PC2b4f98" #blue
    echo -en "\e]P5706c9a" #darkmagenta
    echo -en "\e]PD826ab1" #magenta
    echo -en "\e]P692b19e" #darkcyan
    echo -en "\e]PEa1cdcd" #cyan
    echo -en "\e]P7ffffff" #lightgrey
    echo -en "\e]PFdedede" #white
   clear #for background artifacting
fi

if [ "$TERM" = "xterm-ghostty" ]; then
    export TERM="xterm-256color"
fi

# User specific aliases and functions
alias backlight="~/scripts/backlight_control.sh"
alias la="ls -lh --group-directories-first --color=yes"
alias la="ls -alh --group-directories-first --color=yes"
alias ls="ls --group-directories-first --color=yes"
alias ga="git add -ip"
alias gb="git branch -a"
alias gc="git commit -S"
alias gd="git diff"
alias gconflict="git diff --name-only --diff-filter=u"
alias gp="git push"
alias gs="git state"
alias git_updatesubs="git submodule update --init --recursive && \
  git submodule update --recursive --remote"
alias google="/home/reddraconi/scripts/google.sh"
alias moonlight="flatpak run com.moonlight_stream.Moonlight"
alias murder="kill -9"
alias gopen="gnome-open"
alias pmc="podman-compose"
alias pm="podman"
alias sops="sops --disable-version-check"
alias spot="flatpak run io.github.hrkfdn.ncspot"
alias tf="terraform"
alias ":q"="exit"
alias ..="cd .."

alias mdl="npm exec markdownlint"

if [[ $(which steam >/dev/null 2>&1; echo $?) -eq 0 ]]; then
  alias steam="export STEAM_FRAME_FORCE_CLOSE=1; $(which steam) -forcedesktopscaling 1.15 %U"
fi

if [[ $(which apt >/dev/null 2>&1; echo $?) -ne 0 ]]; then
  alias aptall="sudo apt update && sudo apt upgrade && sudo apt autoremove"
fi

## PS1 Prompt Configuration ####################################################

if [ "$(echo "${BASH_VERSION}" | cut -d. -f1)" -ge 4 ]; then
  # This places an ellipsis between the first and n last directories when
  # the /w or /W flags are used below. Requires bash 4 or newer
  export PROMPT_DIRTRIM=3
fi

# PS1 setup. ANSI color codes are below so it's easier to handle
# This uses 88/256 colors.
c1="\[\033[38;5;7m\]"     # Bright Grey
c2="\[\033[38;5;4m\]"     # Blue
c3="\[\033[38;5;2m\]"     # Green
c4="\[\033[38;5;9m\]"     # Red
c5="\[\033[38;5;13m\]"    # Purple/Magenta
bold="\[\033[1m"
resetc="\[\033[0m\]" # Reset

# If TMUX is active, we don't need a noisy PS1 prompt. All of the same info is
# available in the statusbar.

if [ -n "$TMUX" ]; then
    export PS1="${c1}[${c5}\w${c1}]\n\_->\$${resetc}"
elif [ -z "$WINDIR" ]; then
    user_at_host="${c1}[${c2}\u${c1}@${c3}\h${c1}]"
    date_time="${c1}[${c4}\D{%a %Y%m%d}${c1}]"
    working_dir="${c1}[${c5}\w${c1}]"

    chassis=$(hostnamectl status | grep Chassis | cut -f2 -d":" | tr -d " ")

    if [ "$chassis" == "laptop" ]; then
      batt="-${c1}[${c3}\$(acpi |head -n1| cut -d' ' -f3,4,5 | \
        sed -e 's/Charging/${bold}\+${resetc}/' \
            -e 's/Discharging/${c4}${bold}-${resetc}/' \
            -e 's/Full/${c1}${bold}F${resetc}/' \
            -e 's/,//g' | cut -d':' -f1,2)${c1}]"
    else
      batt=""
    fi

    export PS1="\n$user_at_host$batt-$date_time-$working_dir\n\_->\$$resetc"
fi

# Export user-default editor stuff.
export VISUAL=vim
export EDITOR="$VISUAL"
GPG_TTY=$(tty)
export GPG_TTY

if [[ -n "$SSH_CONNECTION" ]]; then
  export PINENTRY_USER_DATA="USE_CURSES=1"
fi

# Set local .dircolors usage
if [ -f "$HOME/.dir_colors" ]; then
  eval "$(dircolors ~/.dir_colors)"
fi

if [ -f /usr/lib/systemd/user/podman.socket ]; then
  export DOCKER_HOST=unix:///run/usr/$UID/podman/podman.sock
  alias docker="podman"
fi

export ENVDIR=$HOME/Documents/environment
export PATH=$PATH:~/.local/bin:$ENVDIR/scripts/:~/.cargo/bin

# Clean up after ourselves
unset c1
unset c2
unset c3
unset c4
unset c5
unset c6
unset c7
unset resetc
